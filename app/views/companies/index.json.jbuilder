json.array!(@companies) do |company|
  json.extract! company, :id, :name, :business_id, :home_page
  json.url company_url(company, format: :json)
end
