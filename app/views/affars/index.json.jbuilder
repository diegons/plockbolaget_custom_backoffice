json.array!(@affars) do |affar|
  json.extract! affar, :id, :koncept_id, :namn, :internal_id, :internal_id_txt, :dl_kod, :telefon, :fax, :adress_id
  json.url affar_url(affar, format: :json)
end
