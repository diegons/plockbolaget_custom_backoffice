json.array!(@custom_campaigns) do |custom_campaign|
  json.extract! custom_campaign, :id, :name, :description, :company_id, :start_date, :end_date, :notes, :num_images_required
  json.url custom_campaign_url(custom_campaign, format: :json)
end
