json.array!(@koncepts) do |koncept|
  json.extract! koncept, :id, :namn, :kedja_id
  json.url koncept_url(koncept, format: :json)
end
