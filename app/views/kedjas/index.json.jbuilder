json.array!(@kedjas) do |kedja|
  json.extract! kedja, :id, :namn, :internal_id
  json.url kedja_url(kedja, format: :json)
end
