json.array!(@excel_to_model_mappings) do |excel_to_model_mapping|
  json.extract! excel_to_model_mapping, :id, :model, :excel_kolumn_titel, :model_attribute
  json.url excel_to_model_mapping_url(excel_to_model_mapping, format: :json)
end
