    var datagridProps = {
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false,
            loadAtStart: false
        },

        url: "/shops/",
        selection: true,
        multiSelect: true,
        rowSelect: true,
        keepSelection: true,
        formatters: {
            nestedValueGetter: function(column, row) {
                if (column.id === 'shop_chains.name') {
                    return row.concept.chain.name;
                } else if (column.id === 'shop_concepts.name') {
                    return row.concept.name;
                } else if (column.id === 'shops.name') {
                    return row.name;
                }
            }
        }
    };

    var onSelectedEvent = function(e, rows) {
        var isAddShopsBtnDisable = $("#datagrid-toSelect").bootgrid("getSelectedRows").length == 0;
        setButtonDisableStatus("addShopsBtn", isAddShopsBtnDisable);
    }

    var onDeselectedEvent = onSelectedEvent;

    function loadDatagrids() {
        $("#datagrid-toSelect").bootgrid(datagridProps).on("selected.rs.jquery.bootgrid", onSelectedEvent).on("deselected.rs.jquery.bootgrid", onDeselectedEvent);
        $("#datagrid-selected").bootgrid(datagridProps);

    }