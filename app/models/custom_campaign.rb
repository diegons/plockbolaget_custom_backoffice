class CustomCampaign < ActiveRecord::Base
  belongs_to :company
  belongs_to :customer_responsible, :foreign_key => "customer_responsible_id", :class_name => "Person"
  belongs_to :internal_responsible, :foreign_key => "internal_responsible_id", :class_name => "Person"

  has_and_belongs_to_many :affars
  has_and_belongs_to_many :products
end
