class Person < ActiveRecord::Base
	belongs_to :company
	has_many :custom_campaigns
	
	validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
end
