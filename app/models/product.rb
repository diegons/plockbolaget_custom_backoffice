class Product < ActiveRecord::Base
  belongs_to :manufacturer
  has_and_belongs_to_many :custom_campaigns
  belongs_to :manufacturer, :foreign_key => 'manufacturer_id', :class_name => 'Company'
end
