require 'spreadsheet'

class ExcelDataImporter
  
  def initialize(excel_filepath, content_type, model_class_name)
  	@result_ok = [0, "Ok"]
    @excel_filepath = excel_filepath
    @content_type = content_type
    @model_class_name = model_class_name
    @mappings_map = Hash.new
    mappings = ExcelToModelMapping.where(model: model_class_name)
    mappings.each { |mapping| @mappings_map[mapping.excel_kolumn_titel.to_s] = mapping.model_attribute }
  end

  def validate

  	if @mappings_map.empty?
  		return [-1, "Det finns ingen konfiguration i databasen för att importera excel filen till modelen: #{@model_class_name}"]
  	elsif @content_type != "application/vnd.ms-excel" 
  		return [-1, "Den bifogade filen är inte Excel typ."]		
  	end

    book = Spreadsheet.open(@excel_filepath)
    sheet1 = book.worksheet(0)
    headers = sheet1.row(0)

    missing_keys = []

    @mappings_map.keys.each do |mapping_header|
      if !headers.include?(mapping_header)
        missing_keys << mapping_header
      end
    end

    if !missing_keys.empty?
      return [-1, "Kolumner #{missing_keys.join(', ')} saknas i excel filen. Kolla att data i filen är associerad till #{@model_class_name}."]
    end

    return @result_ok

  end

  def import_data

  	result = validate
  	return result if result.first != 0

    book = Spreadsheet.open(@excel_filepath)
    sheet1 = book.worksheet(0)
    headers = sheet1.row(0)
   
    sheet1.rows.each do |row|
      model_instance = Object.const_get(@model_class_name).new
      missing_attributes_in_model = []

      row.each do |column|

        header_for_column = headers[row.index(column)]

        if @mappings_map.has_key?(header_for_column)
          model_attribute = @mappings_map[header_for_column]

          #puts "Column index #{row.index(column)}, header #{header_for_column}, model_attribute #{model_attribute}"

          if set_attribute_if_exists(model_instance, model_attribute, column)
          	#TODO We need to use Plockbolaget's internal_id to identify if a shop (or other data) already exists in
          	#the database. Then instead of creating the record, we need to update it.
          	model_instance.save
          else
            missing_attributes_in_model << model_attribute
          end
        end
      end

	  unless missing_attributes_in_model.empty?
        return [-1, "Attributer [#{missing_attributes_in_model.join(', ')}] saknas i modelen."]  
      end
      
    end

    return @result_ok
  end

  def set_attribute_if_exists(model_instance, attribute, value)

  	unless attribute.include?(".")
  		if model_instance.has_attribute?(attribute)
  			model_instance.send("#{attribute}=", value)
  			return true
  		end

  		return false
  	end

  	# We handle associations

  	attributes = attribute.split(".")
  	parent_class_name = attributes.first.capitalize
  	parent_instance = Object.const_get(parent_class_name).new
  	attributes_tail = attributes[1, attributes.length].join(".")

  	set_attribute_if_exists(parent_instance, attributes_tail, value) 

  end

end
