class Affar < ActiveRecord::Base
  belongs_to :koncept
  belongs_to :adress
  has_and_belongs_to_many :custom_campaigns
end
