class AffarsController < ApplicationController
  before_action :set_affar, only: [:show, :edit, :update, :destroy]

  # GET /affars
  # GET /affars.json
  def index
    @affars = Affar.all
  end

  # GET /affars/1
  # GET /affars/1.json
  def show
  end

  # GET /affars/new
  def new
    @affar = Affar.new
  end

  # GET /affars/1/edit
  def edit
  end

  # POST /affars
  # POST /affars.json
  def create
    @affar = Affar.new(affar_params)
    @affar.adress = Adress.new(adress_params)

    #TODO: Must be done as transaction when saving the shop
    @affar.adress.save
    
    respond_to do |format|
      if @affar.save
        format.html { redirect_to @affar, notice: 'Affar was successfully created.' }
        format.json { render :show, status: :created, location: @affar }
      else
        format.html { render :new }
        format.json { render json: @affar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /affars/1
  # PATCH/PUT /affars/1.json
  def update
    #TODO: Must be done as transaction when saving the shop
    @affar.adress.update(adress_params)

    respond_to do |format|
      if @affar.update(affar_params)
        format.html { redirect_to @affar, notice: 'Affar was successfully updated.' }
        format.json { render :show, status: :ok, location: @affar }
      else
        format.html { render :edit }
        format.json { render json: @affar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /affars/1
  # DELETE /affars/1.json
  def destroy
    @affar.destroy
    respond_to do |format|
      format.html { redirect_to affars_url, notice: 'Affar was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_affar
      @affar = Affar.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def affar_params
      params.require(:affar).permit(:koncept_id, :namn, :internal_id, :internal_id_txt, :dl_kod, :telefon, :fax, :adress_id)
    end

    def adress_params
      params.require(:adress).permit(:gatan, :post_num, :kommun, :ort)
    end
end
