class CustomCampaignsController < ApplicationController
  before_action :set_custom_campaign, only: [:show, :edit, :update, :destroy]

  # GET /custom_campaigns
  # GET /custom_campaigns.json
  def index
    @custom_campaigns = CustomCampaign.all
  end

  # GET /custom_campaigns/1
  # GET /custom_campaigns/1.json
  def show
  end

  # GET /custom_campaigns/new
  def new
    @custom_campaign = CustomCampaign.new
  end

  # GET /custom_campaigns/1/edit
  def edit
  end

  # POST /custom_campaigns
  # POST /custom_campaigns.json
  def create
    @custom_campaign = CustomCampaign.new(custom_campaign_params)

    respond_to do |format|
      if @custom_campaign.save
        format.html { redirect_to @custom_campaign, notice: 'Custom campaign was successfully created.' }
        format.json { render :show, status: :created, location: @custom_campaign }
      else
        format.html { render :new }
        format.json { render json: @custom_campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /custom_campaigns/1
  # PATCH/PUT /custom_campaigns/1.json
  def update
    respond_to do |format|
      if @custom_campaign.update(custom_campaign_params)
        format.html { redirect_to @custom_campaign, notice: 'Custom campaign was successfully updated.' }
        format.json { render :show, status: :ok, location: @custom_campaign }
      else
        format.html { render :edit }
        format.json { render json: @custom_campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /custom_campaigns/1
  # DELETE /custom_campaigns/1.json
  def destroy
    @custom_campaign.destroy
    respond_to do |format|
      format.html { redirect_to custom_campaigns_url, notice: 'Custom campaign was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_custom_campaign
      @custom_campaign = CustomCampaign.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def custom_campaign_params
      params.require(:custom_campaign).permit(:name, :description, :company_id, :start_date, :end_date, :notes, :num_images_required)
    end
end
