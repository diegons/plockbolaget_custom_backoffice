class DataImportsController < ApplicationController

  # GET /data_imports/new
  def new
    @data_import = DataImport.new
  end

  
  # POST /data_imports
  # POST /data_imports.json
  def create

    #respond_to do |format|
      #if @data_import.save
        #format.html { redirect_to @data_import, notice: 'Data import was successfully created.' }
        #format.json { render :show, status: :created, location: @data_import }
        #format.json { render :show, status: :created}
      #else
        #format.html { render :new }
        #format.json { render json: @data_import.errors, status: :unprocessable_entity }
      #end
    #end

    respond_to do |format|
      if @data_import.save
        format.js
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def data_import_params
      params.require(:data_import).permit(:file)
    end

end
