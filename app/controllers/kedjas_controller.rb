class KedjasController < ApplicationController
  before_action :set_kedja, only: [:show, :edit, :update, :destroy]

  # GET /kedjas
  # GET /kedjas.json
  def index
    @kedjas = Kedja.all
  end

  # GET /kedjas/1
  # GET /kedjas/1.json
  def show
  end

  # GET /kedjas/new
  def new
    @kedja = Kedja.new
  end

  # GET /kedjas/1/edit
  def edit
  end

  # POST /kedjas
  # POST /kedjas.json
  def create
    @kedja = Kedja.new(kedja_params)

    respond_to do |format|
      if @kedja.save
        format.html { redirect_to @kedja, notice: 'Kedja was successfully created.' }
        format.json { render :show, status: :created, location: @kedja }
      else
        format.html { render :new }
        format.json { render json: @kedja.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kedjas/1
  # PATCH/PUT /kedjas/1.json
  def update
    respond_to do |format|
      if @kedja.update(kedja_params)
        format.html { redirect_to @kedja, notice: 'Kedja was successfully updated.' }
        format.json { render :show, status: :ok, location: @kedja }
      else
        format.html { render :edit }
        format.json { render json: @kedja.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kedjas/1
  # DELETE /kedjas/1.json
  def destroy
    @kedja.destroy
    respond_to do |format|
      format.html { redirect_to kedjas_url, notice: 'Kedja was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kedja
      @kedja = Kedja.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kedja_params
      params.require(:kedja).permit(:namn, :internal_id)
    end
end
