class KonceptsController < ApplicationController
  before_action :set_koncept, only: [:show, :edit, :update, :destroy]

  # GET /koncepts
  # GET /koncepts.json
  def index
    @koncepts = Koncept.all
  end

  # GET /koncepts/1
  # GET /koncepts/1.json
  def show
  end

  # GET /koncepts/new
  def new
    @koncept = Koncept.new
  end

  # GET /koncepts/1/edit
  def edit
  end

  # POST /koncepts
  # POST /koncepts.json
  def create
    @koncept = Koncept.new(koncept_params)

    respond_to do |format|
      if @koncept.save
        format.html { redirect_to @koncept, notice: 'Koncept was successfully created.' }
        format.json { render :show, status: :created, location: @koncept }
      else
        format.html { render :new }
        format.json { render json: @koncept.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /koncepts/1
  # PATCH/PUT /koncepts/1.json
  def update
    respond_to do |format|
      if @koncept.update(koncept_params)
        format.html { redirect_to @koncept, notice: 'Koncept was successfully updated.' }
        format.json { render :show, status: :ok, location: @koncept }
      else
        format.html { render :edit }
        format.json { render json: @koncept.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /koncepts/1
  # DELETE /koncepts/1.json
  def destroy
    @koncept.destroy
    respond_to do |format|
      format.html { redirect_to koncepts_url, notice: 'Koncept was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_koncept
      @koncept = Koncept.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def koncept_params
      params.require(:koncept).permit(:namn, :kedja_id)
    end
end
