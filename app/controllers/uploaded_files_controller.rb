class UploadedFilesController < ApplicationController
  before_action :set_uploaded_file, only: [:show, :edit, :update, :destroy]

  # GET /uploaded_files
  # GET /uploaded_files.json
  def index
    @uploaded_files = UploadedFile.all
  end

  # GET /uploaded_files/1
  # GET /uploaded_files/1.json
  def show
  end

  # GET /uploaded_files/new
  def new
    @uploaded_file = UploadedFile.new
    load_all_models()
  end

  def load_all_models
    # Load all models
    Rails.application.eager_load! # Not loaded by default in development mode
    @available_models = ActiveRecord::Base.descendants
  end

  # GET /uploaded_files/1/edit
  def edit
  end

  # POST /uploaded_files
  # POST /uploaded_files.json
  def create

    puts params[:uploaded_file]
    tmp_file = params[:uploaded_file][:file]
    model_class_name = params[:model][:name]

    @uploaded_file = UploadedFile.new(filename: tmp_file.original_filename, content_type: tmp_file.content_type, location: '/public/uploads/' )
    save_file_as = Rails.root.join('public', 'uploads', tmp_file.original_filename)

    File.open(save_file_as, 'wb') do |file|
      file.write(tmp_file.read)
    end

    respond_to do |format|

      data_importer = ExcelDataImporter.new(save_file_as, @uploaded_file.content_type, model_class_name)
      result = data_importer.import_data()

      #TODO Both processes must work as one transaction
      if result.first == 0 and @uploaded_file.save
        format.html { redirect_to @uploaded_file, notice: 'Uploaded file was successfully created.' }
        format.json { render :show, status: :created, location: @uploaded_file }
      else
        load_all_models()
        @uploaded_file.errors.add(:file, result[1])
        format.html { render :new }
        format.json { render json: @uploaded_file.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_multifiles_test

    uploaded_files = []
    received_files = params[:files]
    received_files.each do |tmp_file|

      @uploaded_file = UploadedFile.new(filename: tmp_file.original_filename, content_type: tmp_file.content_type, location: '/public/uploads/' )

      File.open(Rails.root.join('public', 'uploads', tmp_file.original_filename), 'wb') do |file|
        file.write(tmp_file.read)
      end

      uploaded_files << @uploaded_file

    end

    respond_to do |format|
      if @uploaded_file.save
        #format.html { redirect_to @uploaded_file, notice: 'Uploaded file was successfully created.' }
        format.json { render :show, status: :created, location: uploaded_files }
      else
        format.html { render :new }
        format.json { render json: @uploaded_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /uploaded_files/1
  # PATCH/PUT /uploaded_files/1.json
  def update
    respond_to do |format|
      if @uploaded_file.update(uploaded_file_params)
        format.html { redirect_to @uploaded_file, notice: 'Uploaded file was successfully updated.' }
        format.json { render :show, status: :ok, location: @uploaded_file }
      else
        format.html { render :edit }
        format.json { render json: @uploaded_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /uploaded_files/1
  # DELETE /uploaded_files/1.json
  def destroy
    @uploaded_file.destroy
    respond_to do |format|
      format.html { redirect_to uploaded_files_url, notice: 'Uploaded file was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_uploaded_file
    @uploaded_file = UploadedFile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def uploaded_file_params
    params.require(:uploaded_file).permit(:filename, :location, :type)
  end
end
