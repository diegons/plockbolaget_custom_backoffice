class ExcelToModelMappingsController < ApplicationController
  before_action :set_excel_to_model_mapping, only: [:show, :edit, :update, :destroy]

  # GET /excel_to_model_mappings
  # GET /excel_to_model_mappings.json
  def index
    @excel_to_model_mappings = ExcelToModelMapping.all
  end

  # GET /excel_to_model_mappings/1
  # GET /excel_to_model_mappings/1.json
  def show
  end

  # GET /excel_to_model_mappings/new
  def new
    @excel_to_model_mapping = ExcelToModelMapping.new

    # Load all models
    Rails.application.eager_load! # Not loaded by default in development mode
    @available_models = ActiveRecord::Base.descendants
   
  end

  # GET /excel_to_model_mappings/1/edit
  def edit
  end

  # POST /excel_to_model_mappings
  # POST /excel_to_model_mappings.json
  def create
    @excel_to_model_mapping = ExcelToModelMapping.new(excel_to_model_mapping_params)

    respond_to do |format|
      if @excel_to_model_mapping.save
        format.html { redirect_to @excel_to_model_mapping, notice: 'Excel to model mapping was successfully created.' }
        format.json { render :show, status: :created, location: @excel_to_model_mapping }
      else
        format.html { render :new }
        format.json { render json: @excel_to_model_mapping.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /excel_to_model_mappings/1
  # PATCH/PUT /excel_to_model_mappings/1.json
  def update
    respond_to do |format|
      if @excel_to_model_mapping.update(excel_to_model_mapping_params)
        format.html { redirect_to @excel_to_model_mapping, notice: 'Excel to model mapping was successfully updated.' }
        format.json { render :show, status: :ok, location: @excel_to_model_mapping }
      else
        format.html { render :edit }
        format.json { render json: @excel_to_model_mapping.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /excel_to_model_mappings/1
  # DELETE /excel_to_model_mappings/1.json
  def destroy
    @excel_to_model_mapping.destroy
    respond_to do |format|
      format.html { redirect_to excel_to_model_mappings_url, notice: 'Excel to model mapping was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_excel_to_model_mapping
      @excel_to_model_mapping = ExcelToModelMapping.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def excel_to_model_mapping_params
      params.require(:excel_to_model_mapping).permit(:model, :excel_kolumn_titel, :model_attribute)
    end
end
