class CreateExcelToModelMappings < ActiveRecord::Migration
  def change
    create_table :excel_to_model_mappings do |t|
      t.string :model
      t.string :excel_kolumn_titel
      t.string :model_attribute

      t.timestamps null: false
    end
  end
end
