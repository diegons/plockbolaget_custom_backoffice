class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.integer :business_id
      t.string :home_page

      t.timestamps null: false
    end
  end
end
