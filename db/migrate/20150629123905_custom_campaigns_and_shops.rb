class CustomCampaignsAndShops < ActiveRecord::Migration
  def change
  	create_table :custom_campaigns_shops do |t|
  		t.references :custom_campaigns, index: true
  		t.references :shops, index: true
  	end
  end
end
