class CreateKedjas < ActiveRecord::Migration
  def change
    create_table :kedjas do |t|
      t.string :namn
      t.integer :internal_id

      t.timestamps null: false
    end
  end
end
