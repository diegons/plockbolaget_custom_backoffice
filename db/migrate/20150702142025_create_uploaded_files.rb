class CreateUploadedFiles < ActiveRecord::Migration
  def change
    create_table :uploaded_files do |t|
      t.string :filename
      t.string :location
      t.string :content_type

      t.timestamps null: false
    end
  end
end
