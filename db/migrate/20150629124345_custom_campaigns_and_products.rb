class CustomCampaignsAndProducts < ActiveRecord::Migration
  def change
  	create_table :custom_campaigns_products do |t|
  		t.references :custom_campaign, index: true
  		t.references :product, index: true
  	end
  end
end
