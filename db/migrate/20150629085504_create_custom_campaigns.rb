class CreateCustomCampaigns < ActiveRecord::Migration
  def change
    create_table :custom_campaigns do |t|
      t.string :name
      t.text :description
      t.references :company, index: true, foreign_key: true
      t.references :customer_responsible, index: true
      t.references :internal_responsible, index: true
      t.date :start_date
      t.date :end_date
      t.text :notes
      t.integer :num_images_required
      t.references :shops
      t.timestamps null: false
    end
  end
end
