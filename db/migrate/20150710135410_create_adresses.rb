class CreateAdresses < ActiveRecord::Migration
  def change
    create_table :adresses do |t|
      t.string :gatan
      t.integer :post_num
      t.string :ort
      t.string :kommun

      t.timestamps null: false
    end
  end
end
