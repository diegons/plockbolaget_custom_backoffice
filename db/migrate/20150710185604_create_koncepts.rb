class CreateKoncepts < ActiveRecord::Migration
  def change
    create_table :koncepts do |t|
      t.string :namn
      t.references :kedja, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
