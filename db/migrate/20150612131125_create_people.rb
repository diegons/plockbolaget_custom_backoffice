class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.references :company, index: true, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :telephone
      t.string :address
    
      t.timestamps null: false
    end
  end
end
