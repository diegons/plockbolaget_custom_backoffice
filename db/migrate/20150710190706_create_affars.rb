class CreateAffars < ActiveRecord::Migration
  def change
    create_table :affars do |t|
      t.references :koncept, index: true, foreign_key: true
      t.string :namn
      t.integer :internal_id
      t.string :internal_id_txt
      t.integer :dl_kod
      t.string :telefon
      t.string :fax
      t.references :adress, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
