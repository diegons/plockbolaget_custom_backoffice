# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150710190706) do

  create_table "adresses", force: :cascade do |t|
    t.string   "gatan"
    t.integer  "post_num"
    t.string   "ort"
    t.string   "kommun"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "affars", force: :cascade do |t|
    t.integer  "koncept_id"
    t.string   "namn"
    t.integer  "internal_id"
    t.string   "internal_id_txt"
    t.integer  "dl_kod"
    t.string   "telefon"
    t.string   "fax"
    t.integer  "adress_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "affars", ["adress_id"], name: "index_affars_on_adress_id"
  add_index "affars", ["koncept_id"], name: "index_affars_on_koncept_id"

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.integer  "business_id"
    t.string   "home_page"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "custom_campaigns", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "company_id"
    t.integer  "customer_responsible_id"
    t.integer  "internal_responsible_id"
    t.date     "start_date"
    t.date     "end_date"
    t.text     "notes"
    t.integer  "num_images_required"
    t.integer  "shops_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "custom_campaigns", ["company_id"], name: "index_custom_campaigns_on_company_id"
  add_index "custom_campaigns", ["customer_responsible_id"], name: "index_custom_campaigns_on_customer_responsible_id"
  add_index "custom_campaigns", ["internal_responsible_id"], name: "index_custom_campaigns_on_internal_responsible_id"

  create_table "custom_campaigns_products", force: :cascade do |t|
    t.integer "custom_campaign_id"
    t.integer "product_id"
  end

  add_index "custom_campaigns_products", ["custom_campaign_id"], name: "index_custom_campaigns_products_on_custom_campaign_id"
  add_index "custom_campaigns_products", ["product_id"], name: "index_custom_campaigns_products_on_product_id"

  create_table "custom_campaigns_shops", force: :cascade do |t|
    t.integer "custom_campaigns_id"
    t.integer "shops_id"
  end

  add_index "custom_campaigns_shops", ["custom_campaigns_id"], name: "index_custom_campaigns_shops_on_custom_campaigns_id"
  add_index "custom_campaigns_shops", ["shops_id"], name: "index_custom_campaigns_shops_on_shops_id"

  create_table "excel_to_model_mappings", force: :cascade do |t|
    t.string   "model"
    t.string   "excel_kolumn_titel"
    t.string   "model_attribute"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "kedjas", force: :cascade do |t|
    t.string   "namn"
    t.integer  "internal_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "koncepts", force: :cascade do |t|
    t.string   "namn"
    t.integer  "kedja_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "koncepts", ["kedja_id"], name: "index_koncepts_on_kedja_id"

  create_table "people", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "telephone"
    t.string   "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "people", ["company_id"], name: "index_people_on_company_id"

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "manufacturer_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "products", ["manufacturer_id"], name: "index_products_on_manufacturer_id"

  create_table "shop_chains", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "shop_concepts", force: :cascade do |t|
    t.integer  "chain_id"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "shop_concepts", ["chain_id"], name: "index_shop_concepts_on_chain_id"

  create_table "shops", force: :cascade do |t|
    t.integer  "concept_id"
    t.integer  "adress_id"
    t.string   "namn"
    t.integer  "internal_id"
    t.string   "internal_id_txt"
    t.integer  "dl_kod"
    t.string   "telefon"
    t.string   "fax"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "shops", ["concept_id"], name: "index_shops_on_concept_id"

  create_table "uploaded_files", force: :cascade do |t|
    t.string   "filename"
    t.string   "location"
    t.string   "content_type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

end
