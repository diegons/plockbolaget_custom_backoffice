## This is a pure rails version of plockbolaget's backoffice. ##

**GOAL: Develop custom functionality in AAVA is a bit cumbersome, that is why I created this project. So one can develop with all rails available tools and then integrate changes in AAVA**.

*Setting up the environment:*

1. Install rails via RVM (see https://rvm.io/rvm/install)
2. Clone this repo, "git clone https://diegons@bitbucket.org/diegons/plockbolaget_custom_backoffice.git"
3. Move to the repository you just cloned (cd /your_path/plockbolaget_custom_backoffice)
4. Execute "bundle install" .This will install all gems (libraries) needed for this project.
5. Execute "rake db:migrate". This will create the database
6. Insert the data included in db/data_inserts.txt. Use any SQL client, for example SQUirrel or dbvisualizer. 
7. Execute "rails server -p [port]". By defaul rails runs in port 3000, same as AAVA.
8. Open your browser at: http://localhost:[port]