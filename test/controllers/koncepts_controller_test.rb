require 'test_helper'

class KonceptsControllerTest < ActionController::TestCase
  setup do
    @koncept = koncepts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:koncepts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create koncept" do
    assert_difference('Koncept.count') do
      post :create, koncept: { kedja_id: @koncept.kedja_id, namn: @koncept.namn }
    end

    assert_redirected_to koncept_path(assigns(:koncept))
  end

  test "should show koncept" do
    get :show, id: @koncept
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @koncept
    assert_response :success
  end

  test "should update koncept" do
    patch :update, id: @koncept, koncept: { kedja_id: @koncept.kedja_id, namn: @koncept.namn }
    assert_redirected_to koncept_path(assigns(:koncept))
  end

  test "should destroy koncept" do
    assert_difference('Koncept.count', -1) do
      delete :destroy, id: @koncept
    end

    assert_redirected_to koncepts_path
  end
end
