require 'test_helper'

class AffarsControllerTest < ActionController::TestCase
  setup do
    @affar = affars(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:affars)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create affar" do
    assert_difference('Affar.count') do
      post :create, affar: { adress_id: @affar.adress_id, dl_kod: @affar.dl_kod, fax: @affar.fax, internal_id: @affar.internal_id, internal_id_txt.string: @affar.internal_id_txt.string, koncept_id: @affar.koncept_id, namn: @affar.namn, telefon: @affar.telefon }
    end

    assert_redirected_to affar_path(assigns(:affar))
  end

  test "should show affar" do
    get :show, id: @affar
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @affar
    assert_response :success
  end

  test "should update affar" do
    patch :update, id: @affar, affar: { adress_id: @affar.adress_id, dl_kod: @affar.dl_kod, fax: @affar.fax, internal_id: @affar.internal_id, internal_id_txt.string: @affar.internal_id_txt.string, koncept_id: @affar.koncept_id, namn: @affar.namn, telefon: @affar.telefon }
    assert_redirected_to affar_path(assigns(:affar))
  end

  test "should destroy affar" do
    assert_difference('Affar.count', -1) do
      delete :destroy, id: @affar
    end

    assert_redirected_to affars_path
  end
end
