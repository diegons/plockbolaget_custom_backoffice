require 'test_helper'

class DataImportsControllerTest < ActionController::TestCase
  setup do
    @data_import = data_imports(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:data_imports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create data_import" do
    assert_difference('DataImport.count') do
      post :create, data_import: { file: @data_import.file }
    end

    assert_redirected_to data_import_path(assigns(:data_import))
  end

  test "should show data_import" do
    get :show, id: @data_import
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @data_import
    assert_response :success
  end

  test "should update data_import" do
    patch :update, id: @data_import, data_import: { file: @data_import.file }
    assert_redirected_to data_import_path(assigns(:data_import))
  end

  test "should destroy data_import" do
    assert_difference('DataImport.count', -1) do
      delete :destroy, id: @data_import
    end

    assert_redirected_to data_imports_path
  end
end
