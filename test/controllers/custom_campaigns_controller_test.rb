require 'test_helper'

class CustomCampaignsControllerTest < ActionController::TestCase
  setup do
    @custom_campaign = custom_campaigns(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:custom_campaigns)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create custom_campaign" do
    assert_difference('CustomCampaign.count') do
      post :create, custom_campaign: { company_id: @custom_campaign.company_id, description: @custom_campaign.description, end_date: @custom_campaign.end_date, name: @custom_campaign.name, notes: @custom_campaign.notes, num_images_required: @custom_campaign.num_images_required, start_date: @custom_campaign.start_date }
    end

    assert_redirected_to custom_campaign_path(assigns(:custom_campaign))
  end

  test "should show custom_campaign" do
    get :show, id: @custom_campaign
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @custom_campaign
    assert_response :success
  end

  test "should update custom_campaign" do
    patch :update, id: @custom_campaign, custom_campaign: { company_id: @custom_campaign.company_id, description: @custom_campaign.description, end_date: @custom_campaign.end_date, name: @custom_campaign.name, notes: @custom_campaign.notes, num_images_required: @custom_campaign.num_images_required, start_date: @custom_campaign.start_date }
    assert_redirected_to custom_campaign_path(assigns(:custom_campaign))
  end

  test "should destroy custom_campaign" do
    assert_difference('CustomCampaign.count', -1) do
      delete :destroy, id: @custom_campaign
    end

    assert_redirected_to custom_campaigns_path
  end
end
