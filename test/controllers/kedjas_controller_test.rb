require 'test_helper'

class KedjasControllerTest < ActionController::TestCase
  setup do
    @kedja = kedjas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:kedjas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create kedja" do
    assert_difference('Kedja.count') do
      post :create, kedja: { internal_id: @kedja.internal_id, namn: @kedja.namn }
    end

    assert_redirected_to kedja_path(assigns(:kedja))
  end

  test "should show kedja" do
    get :show, id: @kedja
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @kedja
    assert_response :success
  end

  test "should update kedja" do
    patch :update, id: @kedja, kedja: { internal_id: @kedja.internal_id, namn: @kedja.namn }
    assert_redirected_to kedja_path(assigns(:kedja))
  end

  test "should destroy kedja" do
    assert_difference('Kedja.count', -1) do
      delete :destroy, id: @kedja
    end

    assert_redirected_to kedjas_path
  end
end
