require 'test_helper'

class ExcelToModelMappingsControllerTest < ActionController::TestCase
  setup do
    @excel_to_model_mapping = excel_to_model_mappings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:excel_to_model_mappings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create excel_to_model_mapping" do
    assert_difference('ExcelToModelMapping.count') do
      post :create, excel_to_model_mapping: { excel_kolumn_titel: @excel_to_model_mapping.excel_kolumn_titel, model: @excel_to_model_mapping.model, model_attribute: @excel_to_model_mapping.model_attribute }
    end

    assert_redirected_to excel_to_model_mapping_path(assigns(:excel_to_model_mapping))
  end

  test "should show excel_to_model_mapping" do
    get :show, id: @excel_to_model_mapping
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @excel_to_model_mapping
    assert_response :success
  end

  test "should update excel_to_model_mapping" do
    patch :update, id: @excel_to_model_mapping, excel_to_model_mapping: { excel_kolumn_titel: @excel_to_model_mapping.excel_kolumn_titel, model: @excel_to_model_mapping.model, model_attribute: @excel_to_model_mapping.model_attribute }
    assert_redirected_to excel_to_model_mapping_path(assigns(:excel_to_model_mapping))
  end

  test "should destroy excel_to_model_mapping" do
    assert_difference('ExcelToModelMapping.count', -1) do
      delete :destroy, id: @excel_to_model_mapping
    end

    assert_redirected_to excel_to_model_mappings_path
  end
end
